<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\User;

class UserController extends Controller
{
  public function index(){
    $users = User::orderBy('created_at', 'desc')->get();//mengurutkan berdasarkan elemen yg paling baru dibuat
    //hasil query berupa collection
    return UserResource::collection($users);
  }

  public function store(Request $request){
    $user = User::create([
      'name' => $request->name
    ]);

    //return $todo;
    return new UserResource($user);//hanya menampilkan satu objek,yg ditampilkan sesuai dg yg diatur pada TodoResource
  }

  public function delete($id){
    User::destroy($id);
    return 'User data successfully deleted';
  }

  public function update(Request $request, $id){
    $user = User::find($id);

    $user->update([
      'name' => $request->name
    ]);

    return new UserResource($user);//hanya menampilkan satu objek, bukan keseluruhan collection

  }
}
