<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ajax Request</title>
  </head>
  <body>

    <!-- <style>
        li {
            list-style: none;
        }
    </style> -->

    <div id="userapp">
        <h2>Welcome to UserApp!</h2>

        <input type="text" v-model="username" id="userName"> </input>
        <button v-on:click="addUser" v-show="statusAdd">Add User</button>
        <button v-on:click="updateUser" v-show="statusUpdate">Update</button>

        <ul>
            <li v-for="(user, index) in users">
              <span>@{{ user.name }}</span>  ||
              <button v-on:click="editUserName(index)"> Edit </button> ||
              <button v-on:click="deleteUser(index, user)"> Delete </button>
            </li>
        </ul>

    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"> </script>
    <!-- CDN untuk Resource Vue -->
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

    <script>
        var vm = new Vue({
        el : '#userapp',
        data : {
          users : [],
          userid: "",
          username: "",
          user_index: "",
          statusAdd: true,
          statusUpdate: false,
        },
        methods : {
            addUser: function () {
              this.$http.post('/api/index', { name: this.username }).then(response => {//mengambil data dari hasil route API

                  return this.users.unshift({"name": this.username,});
                  this.username = "";

              });

            },
            editUserName: function (index) {
                this.statusAdd = false;
                this.statusUpdate = true;
                this.user_index = index;
                this.username = this.users[index].name;
                this.userid = this.users[index].id;
            },
            deleteUser: function (index, user) {

              this.$http.post('/api/index/delete/' + user.id).then(response => {

                if (confirm("Apakah anda yakin?")) {
                  this.users.splice(index, 1);
                } else {

                }

              });

            },
            updateUser: function (index) {
              this.$http.post('/api/index/update/' + this.userid, { name: this.username }).then(response => {

                  Vue.set(vm.users, this.user_index, {"name":this.username,});
                  this.statusAdd = true;
                  this.statusUpdate = false;
                  this.user_index = "";
                  this.username = "";

              });

            }
        },
        watch: {
            username: function (val) {
                this.username = val
            }
        },
        //ketika id #app dirender maka fungsi berikut akan dijalankan
        mounted : function(){
          // GET /someUrl
          this.$http.get('/api/index').then(response => {//mengambil data dari hasil route API
            let result = response.body.data;
            this.users = result;

            //response.body mengembalikan response dalam bentuk body "data" beserta isinya (array)
            console.log(response.body.data);//menegmbailkan dalam bentuk response yg berisi array dari body "data"

          });
        }
    });
    </script>
  </body>
</html>
